package com.pcapps.controllers;

import com.pcapps.dto.JsonProduct;
import com.pcapps.model.Product;
import com.pcapps.services.MarketService;
import com.pcapps.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;


@RestController
@RequestMapping(value = "restapi/v1/")
public class ProductController {

    private final ProductService productsService;

    private final MarketService marketService;

    @Autowired
    public ProductController(ProductService productsService, MarketService marketService) {
        this.productsService = checkNotNull(productsService, "productService == null");
        this.marketService = checkNotNull(marketService, "MarketServices is nul");
    }

    //@info:I spent a lot time to work with this stupid headers@!!!
    //dont work with this ,consumes = "application/x-www-form-urlencoded"
    //no fucking idea why and also i need to send empty content typ header

    /*
    Druga spraw jest taka podajemy model attribute czy jest to poprawne ?
    Skoror jest to rest ro czy nagłówki nie powinny akcpetować jsona ?
    CZy jezeli wysyłąmy plik to jest potrzebny inny nagłówek niż multipart ?
    Dlaczego spring odrzuca wszystkie requesty ktore nie maja Content-Type
     */
    @PostMapping(value = "insertImage", headers = "content-type=multipart/*")
    public Product insertImage(@Valid @ModelAttribute JsonProduct product) throws IOException {
        return productsService.saveFilesConvertAndStoreToDb(product);
    }

    @GetMapping(value = "{idProduct}")
    public Product get(@PathVariable Long idProduct) {
        Product asd = productsService.getOne(idProduct);
        return asd;
    }

    @PutMapping
    public Product put(Product product) {
        //TODO: implement me!
        return null;
    }

    @PostMapping
    public Product post(Product product) {
        //TODO: implement me!
        return null;
    }

    @PatchMapping
    public Product patch(Product product) {
        //TODO: implement me!
        return null;
    }

    @DeleteMapping(value = "{idProduct}")
    public Product delete(@PathVariable int idProduct) {
        //TODO: implement me!
        return null;
    }

//    @ExceptionHandler(Exception.class)
//    @ResponseStatus(HttpStatus.NOT_FOUND)
//    public void chuj(Exception e) {
//        System.out.println("Sorry some error occured");
//    }

}
