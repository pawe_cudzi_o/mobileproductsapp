package com.pcapps.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aspect
@Component
public class LoggerAspect {
    private final Logger logger = Logger.getLogger(getClass().getName());

    @Before("execution(* com..*.*(..))")
    public void logSetMethodsCalls(JoinPoint joinPoint) {
        String className = joinPoint.getSignature().getDeclaringTypeName();
        String classMethod = joinPoint.getSignature().getName();
        logger.info(String.format("The class %s called method %s", className, classMethod));
    }

}
