package com.pcapps.services;

import com.pcapps.converter.ProductDtoToProduct;
import com.pcapps.dto.JsonProduct;
import com.pcapps.model.Product;
import com.pcapps.repository.ProductRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service

public class ProductService {


    private final ProductRepository productRepository;
    private final ProductDtoToProduct productDtoToProduct;

    public ProductService(ProductRepository productRepository, ProductDtoToProduct productDtoToProduct) {
        this.productRepository = productRepository;
        this.productDtoToProduct = productDtoToProduct;
    }

    public Product save(Product product) {
        return productRepository.save(product);
    }


    public List<Product> getAll() {
        return productRepository.findAll();
    }

    @Transactional
    public Product saveFilesConvertAndStoreToDb(JsonProduct product) {
        return productRepository.save(productDtoToProduct.convert(product));
    }

    //todo:To jest delefacja metod. No nie ?
    //Ale robi sie tak bo logika moze byc kiedys większa ?? True ?
    public Product getOne(Long id) {
        return productRepository.getOne(id);
    }
}
