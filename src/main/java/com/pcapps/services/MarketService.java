package com.pcapps.services;


import com.pcapps.model.Market;
import com.pcapps.repository.MarketRepositoryInterface;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Scope("prototype")
public class MarketService {


    //@info: I can use Autowire and primary if i want to use some special implementation of intreface
    //or i can user Resource for getting bean name or @Qualifier and this look all :)

    //    @Qualifier("marketRepositoryInterface")
    //    @Autowired
    @Resource(name = "marketServiceInterface")
    private MarketRepositoryInterface marketRepository;


    public List<Market> getAll() {
        return marketRepository.someCustomQueryWithJpa();
    }

    public Market getSingle(Long id) {
        return marketRepository.findOne(id);
    }
}
