package com.pcapps.file;

import java.io.File;

abstract class AbstractStoreFile {


    private final byte bytes[];
    private final String dir;

    AbstractStoreFile(byte[] bytes, String dir) {
        this.bytes = bytes;
        this.dir = dir;

    }

    public byte[] getBytes() {
        return bytes;
    }

    public String getDir() {
        return dir;
    }

    abstract File store();
}
