package com.pcapps.file;

import net.bytebuddy.utility.RandomString;
import org.springframework.stereotype.Component;

import java.io.*;


class StoreFile extends AbstractStoreFile {

    StoreFile(byte[] bytes, String dir) {
        super(bytes, dir);
    }

    File store() {
        File serverFile = new File(getDir() + File.separator + "pawel" + RandomString.make(7) + ".jpg");
        BufferedOutputStream stream = null;
        try {
            stream = new BufferedOutputStream(new FileOutputStream(serverFile));
            stream.write(getBytes());
            stream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return serverFile;
    }

}
