package com.pcapps.file;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@PropertySource("classpath:file/file-storing.properties")
public class FileService {

    /*
    todo:

    Provider ? -> provide me a files after store ?
    Procesor ? -> process multipart data and return me files ?

    Jaka nazwa lepsza ? czy jescze jakas inna moze być >
     */

    @Value("${store.directory}")
    private String dir;

    @Value("${store.provider}")
    private String storeFileProviderName;

    public String getDir() {
        return dir;
    }


    public List<File> storeMultipleFiles(List<MultipartFile> multipartFileList) throws IOException {
        List<File> fileList = new ArrayList();
        for (MultipartFile multipartFile : multipartFileList) {
            //todo:I think we can have less coupling multipartFile.getBytes
            fileList.add(creteFileProvider(multipartFile.getBytes(), dir).get().store());
        }
        return fileList;
    }

    private Optional<AbstractStoreFile> creteFileProvider(byte[] bytes, String dir) {
        AbstractStoreFile processor = null;
        try {
            Constructor o = Class.forName(storeFileProviderName).getDeclaredConstructor(byte[].class, String.class);
            processor = (AbstractStoreFile) o.newInstance(bytes, dir);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return Optional.of(processor);
    }

}
