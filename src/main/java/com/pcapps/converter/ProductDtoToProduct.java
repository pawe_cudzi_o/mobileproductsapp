package com.pcapps.converter;

import com.pcapps.dto.JsonProduct;
import com.pcapps.file.FileService;
import com.pcapps.model.Images;
import com.pcapps.model.Product;
import com.pcapps.services.MarketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductDtoToProduct {

    private final FileService fileService;

    private final Product product;


    /*
    todo:nie wiem czy to nie jest słabe umieszczac tutaj serwis. ALe nie wiem do konca jak to przekonwertowac
     albo jakos wstrzyknać market w locie. Albo moze jest gotowe rozwiązanie do tego.
     Wiem ze ta  konwersja powinan być gdzies na wysokosci serwisu.

    Wstrzykiwanie tu serwisu sprawia ze klasa ma juz dwie inne klasy na których polega FileService I marketService.
     */
    private final MarketService marketService;

    @Autowired
    public ProductDtoToProduct(FileService fileService, MarketService marketService) {
        this.marketService = marketService;
        this.fileService = fileService;
        //todo:very bad ? to make new Product in constuctor ?
        product = new Product();
    }

    public Product convert(JsonProduct jsonProduct) {
        product.setImagesList(saveDataToFileAndReturnPathList(jsonProduct.getFiles()));
        product.setMarket(marketService.getSingle(jsonProduct.getMarket()));
        product.setName(jsonProduct.getName());
        return product;
    }

    private List<Images> saveDataToFileAndReturnPathList(List<MultipartFile> multipartFiles) {

        List<Images> imagesList = new ArrayList<>();
        try {
            List<File> fileList = fileService.storeMultipleFiles(multipartFiles);

            imagesList = fileList.stream().map(file -> {
                Images image = new Images();
                image.setPath(file.getPath());
                return image;
            }).collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return imagesList;
    }
}
