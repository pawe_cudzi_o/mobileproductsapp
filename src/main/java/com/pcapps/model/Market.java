package com.pcapps.model;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "markets")
@NamedQueries({
        @NamedQuery(name = Market.FIND_ALL_MARKETS, query = "Select m From Market m")
})
public class Market {

    public static final String FIND_ALL_MARKETS = "findAllMarkets";

    @GeneratedValue
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "market", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Product> products = new LinkedList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
