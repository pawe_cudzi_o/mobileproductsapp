package com.pcapps.repository;


import com.pcapps.model.Market;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class MarketRepository {

    @PersistenceContext()
    private EntityManager em;


    public Market save(Market market) {

        if (market.getId() != null) {

        }
        em.persist(market);
        em.flush();
        return market;
    }

    public List<Market> fetchAll() {
        TypedQuery<Market> query = em.createNamedQuery(Market.FIND_ALL_MARKETS, Market.class);
        return query.getResultList();
    }

}
