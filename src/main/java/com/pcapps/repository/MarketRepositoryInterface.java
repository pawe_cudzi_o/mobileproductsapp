package com.pcapps.repository;

import com.pcapps.model.Market;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


//Using Spring Data Jpa

@Repository("marketServiceInterface")
public interface MarketRepositoryInterface extends JpaRepository<Market, Long> {

    @Query("Select m From Market m")
    public List<Market> someCustomQueryWithJpa () ;

}
